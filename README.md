*******  *******

Upload and share maps such as routes, placemarks etc using Google Maps.
 
This plugin offers users the option to upload and share map files (kml or kmz) or insert and share maps directly from Google Maps by using share "Link" option.

Sharemaps Plugin is useful for travel communities as well as activity communities such as motorcyclists, bikers, runners, hikers, climbers etc


== Contents ==
1. Features
2. Installation
3. ToDo
4. Pro Version


== 1. Features ==
- Google maps integration
- Upload google kml or kmz files
- Download map files shared by other users
- Insert map link directly from Google Maps, without need of uploading files
- English, Greek and Spanish language files


== 2. Installation ==
Requires: Elgg 1.8 or higher

1. Upload sharemaps in "/mod/" elgg folder
2. Important: make directory 'mod/sharemaps/maps' writable from web server
3. Activate the plugin in the administration panel
4. Ensure that images at 'mod/sharemaps/graphics' are readable from web server
5. In 'Administration/Configure/Settings/Share Maps' you can configure several map options


== 3. ToDo ==
- Option in settings for enable/disable uploading files, sharing map links or both (now both are enabled)
- More options in general


== 4. Pro Version ==
Sharemaps Pro offers all features of free version plus the option of uploading gpx files (directly from GPS Trackers). You can find Sharemaps Pro at http://elgg.lyberakis.gr/elgg-plugins/sharemaps
